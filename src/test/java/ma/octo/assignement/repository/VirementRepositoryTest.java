package ma.octo.assignement.repository;

import ma.octo.assignement.models.Virement;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Transactional
public class VirementRepositoryTest {

  @Autowired
  private VirementRepository virementRepository;

  @Test
  public void findOne() {
    Long idVirement = 6L;
    Virement virement = new Virement(idVirement, new BigDecimal(100), new Date(), null,null, "Testing");
    virementRepository.save(virement);
    virement = virementRepository.findById(idVirement).orElse(null);
    System.out.println(virement);
    assertNotEquals(virement, null);
  }

  @Test
  public void findAll() {

    List<Virement> virementList =  virementRepository.findAll();
    assertEquals(virementList.size(),1);
  }

  @Test
  public void save() {
    Long idVirement = 6L;
    Virement virement = new Virement(idVirement, new BigDecimal(100), new Date(), null,null, "Testing");
    assertNotEquals(virementRepository.save(virement), null);
  }

  @Test
  public void delete() {
    virementRepository.deleteById(5L);
    assertEquals(virementRepository.findAll().size(),0);
  }
}