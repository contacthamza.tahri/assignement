package ma.octo.assignement.mapper;

import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.models.Compte;
import ma.octo.assignement.service.CompteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CompteMapper {

    public CompteDto map(Compte compte) {

        CompteDto compteDto = new CompteDto();

        compteDto.setNrCompte(compte.getNrCompte());
        compteDto.setRib(compte.getRib());
        compteDto.setSolde(compte.getSolde());

        return compteDto;
    }

    public Compte map(CompteDto compteDto) {

        Compte compte = new Compte();

        compte.setNrCompte(compteDto.getNrCompte());
        compte.setRib(compteDto.getRib());
        compte.setSolde(compteDto.getSolde());

        return compte;
    }

    public List<CompteDto> map(List<Compte> comptes) {

        return  comptes
                .stream()
                .map(compte -> map(compte))
                .collect(Collectors.toList());
    }

    public List<Compte> toMap(List<CompteDto> compteDtos) {

        return  compteDtos
                .stream()
                .map(compteDto -> map(compteDto))
                .collect(Collectors.toList());
    }



}
