package ma.octo.assignement.mapper;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.models.Versement;
import ma.octo.assignement.models.Virement;
import ma.octo.assignement.service.CompteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class VersementMapper {

    @Autowired
    CompteService compteService;

    public VersementDto map(Versement versement) {

        VersementDto versementDto = new VersementDto();
        versementDto.setNrCompteBeneficiaire(versement.getCompteBeneficiaire().getNrCompte());
        versementDto.setNom_prenom_emetteur(versement.getNom_prenom_emetteur());
        versementDto.setDate(versement.getDateExecution());
        versementDto.setMontantVersement(versement.getMontantVersement());

        return versementDto;
    }

    public Versement map(VersementDto versementDto) {

        Versement versement = new Versement();

        versement.setCompteBeneficiaire(compteService.findByNrCompte(versementDto.getNrCompteBeneficiaire()));
        versement.setNom_prenom_emetteur(versementDto.getNom_prenom_emetteur());
        versement.setDateExecution(versementDto.getDate());
        versement.setMontantVersement(versementDto.getMontantVersement());

        return versement;
    }

    public List<VersementDto> map(List<Versement> versements) {

        return  versements
                .stream()
                .map(versement -> map(versement))
                .collect(Collectors.toList());
    }

    public List<Versement> toMap(List<VersementDto> versementDtos) {

        return  versementDtos
                .stream()
                .map(versementDto -> map(versementDto))
                .collect(Collectors.toList());
    }



}
