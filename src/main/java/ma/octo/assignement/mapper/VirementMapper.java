package ma.octo.assignement.mapper;

import ma.octo.assignement.models.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.service.CompteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class VirementMapper {

    @Autowired
    CompteService compteService;

    public VirementDto map(Virement virement) {

        VirementDto virementDto = new VirementDto();
        virementDto.setNrCompteEmetteur(virement.getCompteEmetteur().getNrCompte());
        //Was messing
        virementDto.setNrCompteBeneficiaire(virement.getCompteBeneficiaire().getNrCompte());
        virementDto.setDate(virement.getDateExecution());
        virementDto.setMontantVirement(virement.getMontantVirement());
        virementDto.setMotif(virement.getMotifVirement());

        return virementDto;
    }

    public Virement map(VirementDto virementDto) {

        Virement virement = new Virement();
        virement.setCompteEmetteur(compteService.findByNrCompte(virementDto.getNrCompteEmetteur()));
        virement.setCompteBeneficiaire(compteService.findByNrCompte(virementDto.getNrCompteBeneficiaire()));
        virement.setDateExecution(virementDto.getDate());
        virement.setMontantVirement(virementDto.getMontantVirement());
        virement.setMotifVirement(virementDto.getMotif());

        return virement;
    }

    public List<VirementDto> map(List<Virement> virements) {

        return  virements
                .stream()
                .map(virement -> map(virement))
                .collect(Collectors.toList());
    }

    public List<Virement> toMap(List<VirementDto> virementDtos) {

        return  virementDtos
                .stream()
                .map(virementDto -> map(virementDto))
                .collect(Collectors.toList());
    }



}
