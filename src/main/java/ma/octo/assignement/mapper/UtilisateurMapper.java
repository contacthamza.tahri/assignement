package ma.octo.assignement.mapper;

import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.models.Utilisateur;
import ma.octo.assignement.service.CompteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UtilisateurMapper {

    public UtilisateurDto map(Utilisateur utilisateur) {

        UtilisateurDto utilisateurDto = new UtilisateurDto();


        utilisateurDto.setBirthdate(utilisateur.getBirthdate());

        utilisateurDto.setGender(utilisateur.getGender());
        utilisateurDto.setLastname(utilisateur.getLastname());
        utilisateurDto.setFirstname(utilisateur.getFirstname());

        return utilisateurDto;
    }

    public Utilisateur map(UtilisateurDto utilisateurDto) {

        Utilisateur utilisateur = new Utilisateur();

        utilisateur.setGender(utilisateurDto.getGender());
        utilisateur.setLastname(utilisateurDto.getLastname());
        utilisateur.setFirstname(utilisateurDto.getFirstname());

        return utilisateur;
    }

    public List<UtilisateurDto> map(List<Utilisateur> utilisateurs) {

        return  utilisateurs
                .stream()
                .map(utilisateur -> map(utilisateur))
                .collect(Collectors.toList());
    }

    public List<Utilisateur> toMap(List<UtilisateurDto> utilisateurDtos) {

        return  utilisateurDtos
                .stream()
                .map(utilisateurDto -> map(utilisateurDto))
                .collect(Collectors.toList());
    }



}
