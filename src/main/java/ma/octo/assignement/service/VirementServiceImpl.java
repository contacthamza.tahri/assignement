package ma.octo.assignement.service;

import ma.octo.assignement.mapper.VirementMapper;
import ma.octo.assignement.models.Compte;
import ma.octo.assignement.models.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.VirementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

import static ma.octo.assignement.utils.HelperFunctions.isSoldSuffisant;

@Service
public class VirementServiceImpl implements VirementService{


    @Value("${MONTANT_MAXIMAL}")
    int  MONTANT_MAXIMAL;

    Logger LOGGER = LoggerFactory.getLogger(VirementService.class);



    @Autowired
    private VirementRepository virementRepository;

    @Autowired
    private AuditServiceImpl auditServiceImpl;

    @Autowired
    private CompteService compteService;

    @Autowired
    private VirementMapper virementMapper;




    @Override
    public List<VirementDto> loadAll() {
        List<Virement> all = virementRepository.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return new ArrayList<>();
        } else {
            return virementMapper.map(all);
        }
    }


    @Override
    public void createTransaction(VirementDto virementDto) throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {

        Compte compteEmetteur = compteService.findByNrCompte(virementDto.getNrCompteEmetteur());
        Compte compteBeneficiaire = compteService
                .findByNrCompte(virementDto.getNrCompteBeneficiaire());

        verifyExceptions(virementDto, compteEmetteur, compteBeneficiaire);


        compteService.transferFrom(compteEmetteur , virementDto.getMontantVirement());
        compteService.transferTo(compteBeneficiaire ,  virementDto.getMontantVirement().doubleValue());



        //Changed to mapper
        virementRepository.save(virementMapper.map(virementDto));

        auditServiceImpl.auditVirement("Virement depuis " + virementDto.getNrCompteEmetteur() + " vers " + virementDto
                .getNrCompteBeneficiaire() + " d'un montant de " + virementDto.getMontantVirement()
                .toString());
    }

    private void verifyExceptions(VirementDto virementDto, Compte compteEmetteur, Compte compteBeneficiaire) throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {
        if (compteEmetteur == null) {
            LOGGER.info("Compte Emitteur Non existant");
            throw new CompteNonExistantException("Compte Emitteur Non existant");
        }

        if (compteBeneficiaire == null) {
            LOGGER.info("Compte Beneficaire Non existant");
            throw new CompteNonExistantException("Compte Beneficaire Non existant");
        }

        if ( virementDto.getMontantVirement() == null  || virementDto.getMontantVirement().doubleValue() == 0.0) {
            LOGGER.info("Montant vide");
            throw new TransactionException("Montant vide");

        } else if (virementDto.getMontantVirement().longValue() < 10L) {

            LOGGER.info("Montant minimal de virement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");

        } else if (virementDto.getMontantVirement().doubleValue() > MONTANT_MAXIMAL) {
            LOGGER.info("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");
        }

        if (virementDto.getMotif().length() < 0) {
            LOGGER.info("Motif vide");
            throw new TransactionException("Motif vide");
        }

        if (!isSoldSuffisant(compteEmetteur.getSolde().doubleValue() , virementDto.getMontantVirement().doubleValue())) {
            LOGGER.error("Solde insuffisant pour l'utilisateur");
            throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
        }
    }

    @Override
    public void save(Virement Virement) {
        virementRepository.save(Virement);
    }
}
