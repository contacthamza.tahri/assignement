package ma.octo.assignement.service;

import ma.octo.assignement.models.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

public interface VirementService {

    List<VirementDto> loadAll();

    void createTransaction(@RequestBody VirementDto virementDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException;

    void save(Virement Virement);
}
