package ma.octo.assignement.service;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.mapper.VersementMapper;
import ma.octo.assignement.models.Compte;
import ma.octo.assignement.models.Versement;
import ma.octo.assignement.models.Virement;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.repository.VersementRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

import static ma.octo.assignement.utils.HelperFunctions.isSoldSuffisant;

@Service
public class VersementServiceImpl implements VersementService{


    @Value("${MONTANT_MAXIMAL}")
    int  MONTANT_MAXIMAL;

    Logger LOGGER = LoggerFactory.getLogger(VersementService.class);

    @Autowired
    private VersementRepository versementRepository;

    @Autowired
    private AuditServiceImpl auditServiceImpl;

    @Autowired
    private CompteService compteService;

    @Autowired
    private VersementMapper versementMapper;


    @Override
    public List<VersementDto> listerVersements() {
        List<Versement> all = versementRepository.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return new ArrayList<>();
        } else {
            return versementMapper.map(all);
        }
    }

    @Override
    public void executerVersement(VersementDto versementDto) throws TransactionException, CompteNonExistantException {

        Compte compteBeneficiaire = compteService
                .findByNrCompte(versementDto.getNrCompteBeneficiaire());

        verifyExceptions(versementDto, compteBeneficiaire);

        compteService.transferTo(compteBeneficiaire ,  versementDto.getMontantVersement().doubleValue());

        versementRepository.save(versementMapper.map(versementDto));

        auditServiceImpl.auditVersement("Versement vers " + versementDto
                .getNrCompteBeneficiaire() + " d'un montant de " + versementDto.getMontantVersement()
                .toString());
    }



    private void verifyExceptions(VersementDto versementDto, Compte compteBeneficiaire) throws CompteNonExistantException, TransactionException {
        if (compteBeneficiaire == null) {
            LOGGER.info("Compte Beneficaire Non existant");
            throw new CompteNonExistantException("Compte Beneficaire Non existant");
        }

        if ( versementDto.getMontantVersement() == null || versementDto.getMontantVersement().doubleValue() == 0.0 ) {
            LOGGER.info("Montant vide");
            throw new TransactionException("Montant vide");
        } else if (versementDto.getMontantVersement().longValue() < 10L) {
            LOGGER.info("Montant minimal de versement non atteint");
            throw new TransactionException("Montant minimal de virement non atteint");

        } else if (versementDto.getMontantVersement().doubleValue() > MONTANT_MAXIMAL) {
            LOGGER.info("Montant maximal de virement dépassé");
            throw new TransactionException("Montant maximal de virement dépassé");
        }
    }
}
