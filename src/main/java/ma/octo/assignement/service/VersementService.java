package ma.octo.assignement.service;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

import java.util.List;

public interface VersementService {

    void executerVersement(VersementDto versementDto) throws SoldeDisponibleInsuffisantException, TransactionException, CompteNonExistantException;

    List<VersementDto> listerVersements();
}
