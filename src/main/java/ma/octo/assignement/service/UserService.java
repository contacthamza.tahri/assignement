package ma.octo.assignement.service;

import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.models.Utilisateur;

import java.util.List;

public interface UserService {
    List<UtilisateurDto> loadAllUtilisateur();
}
