package ma.octo.assignement.service;

import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.models.Compte;

import java.math.BigDecimal;
import java.util.List;

public interface CompteService {
    List<CompteDto> loadAllCompte();

    void saveCompte(Compte compteEmetteur);

    Compte findByNrCompte(String nrCompteEmetteur);

    void transferFrom(Compte compteEmetteur, BigDecimal montantVirement);

    void transferTo(Compte compteBeneficiaire, double montantVirement);
}
