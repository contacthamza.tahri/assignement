package ma.octo.assignement.service;

import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.mapper.CompteMapper;
import ma.octo.assignement.models.Compte;
import ma.octo.assignement.repository.CompteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

@Service
public class CompteServiceImpl implements CompteService {

    @Autowired
    private CompteRepository compteRepository;

    @Autowired
    private CompteMapper compteMapper;

    @Override
    public List<CompteDto>  loadAllCompte() {
        List<Compte> all = compteRepository.findAll();

        if (CollectionUtils.isEmpty(all)) {
            return null;
        } else {
            return compteMapper.map(all);
        }
    }

    public void saveCompte(Compte compte){
        compteRepository.save(compte);
    }

    @Override
    public Compte findByNrCompte(String nrCompteEmetteur) {
        return compteRepository.findByNrCompte(nrCompteEmetteur);
    }

    @Override
    public void transferFrom(Compte compteEmetteur, BigDecimal montantVirement) {
        compteEmetteur.setSolde(compteEmetteur.getSolde().subtract(montantVirement));
        compteRepository.save(compteEmetteur);
    }

    @Override
    public void transferTo(Compte compteBeneficiaire, double montantVirement) {
        compteBeneficiaire.setSolde(new BigDecimal(compteBeneficiaire.getSolde().doubleValue() + montantVirement));
        compteRepository.save(compteBeneficiaire);
    }

}
