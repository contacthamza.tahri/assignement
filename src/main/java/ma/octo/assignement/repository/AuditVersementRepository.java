package ma.octo.assignement.repository;

import ma.octo.assignement.models.AuditVersement;
import ma.octo.assignement.models.AuditVirement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AuditVersementRepository extends JpaRepository<AuditVersement, Long> {
}
