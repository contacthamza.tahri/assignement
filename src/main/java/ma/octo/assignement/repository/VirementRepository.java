package ma.octo.assignement.repository;

import ma.octo.assignement.models.Virement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VirementRepository extends JpaRepository<Virement, Long> {
}
