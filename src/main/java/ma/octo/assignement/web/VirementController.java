package ma.octo.assignement.web;

import ma.octo.assignement.models.Compte;
import ma.octo.assignement.models.Utilisateur;
import ma.octo.assignement.models.Virement;
import ma.octo.assignement.dto.VirementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController(value = "/Virement/virements")
class VirementController {

    Logger LOGGER = LoggerFactory.getLogger(VirementController.class);

    @Autowired
    VirementService virementService;

    @GetMapping("/Virement/listerVirements")
    List<VirementDto> loadAll() {
        return virementService.loadAll();
    }


    @PostMapping("/Virement/executerVirement")
    @ResponseStatus(HttpStatus.CREATED)
    public void createTransaction(@RequestBody VirementDto virementDto)
            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
        virementService.createTransaction(virementDto);
    }
}
