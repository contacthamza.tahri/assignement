package ma.octo.assignement.web;

import ma.octo.assignement.dto.CompteDto;
import ma.octo.assignement.models.Compte;
import ma.octo.assignement.service.CompteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController(value = "/Compte/comptes")
public class CompteController {

    Logger LOGGER = LoggerFactory.getLogger(CompteController.class);

    @Autowired
    CompteService compteService;

    @GetMapping("/Compte/listerComptes")
    List<CompteDto> loadAllCompte() {
        return compteService.loadAllCompte();
    }
}
