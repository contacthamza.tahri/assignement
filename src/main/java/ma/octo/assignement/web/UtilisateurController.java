package ma.octo.assignement.web;

import ma.octo.assignement.dto.UtilisateurDto;
import ma.octo.assignement.models.Utilisateur;
import ma.octo.assignement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController(value = "/User/utilisateurs")
public class UtilisateurController {

    @Autowired
    UserService userService;

    @GetMapping("/User/listerUtilisateurs")
    List<UtilisateurDto> loadAllUtilisateur() {
        return userService.loadAllUtilisateur();
    }

}
