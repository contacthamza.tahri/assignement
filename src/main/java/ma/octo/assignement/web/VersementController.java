package ma.octo.assignement.web;

import ma.octo.assignement.dto.VersementDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.VersementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController(value = "/Versement/versements")
class VersementController {

    Logger LOGGER = LoggerFactory.getLogger(VersementController.class);

    @Autowired
    VersementService versementService;

    @PostMapping("/Versement/executerVersement")
    @ResponseStatus(HttpStatus.CREATED)
    private void executerVersement(@RequestBody VersementDto versementDto) throws TransactionException, CompteNonExistantException, SoldeDisponibleInsuffisantException {
        versementService.executerVersement(versementDto);
    }

    @GetMapping("/Versement/listerVersements")
    @ResponseStatus(HttpStatus.CREATED)
    private List<VersementDto> listerVersements(){
        return versementService.listerVersements();
    }

}
