package ma.octo.assignement.dto;

import lombok.Data;
import ma.octo.assignement.models.Utilisateur;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class CompteDto implements Serializable {

  private String nrCompte;
  private String rib;
  private BigDecimal solde;

}
