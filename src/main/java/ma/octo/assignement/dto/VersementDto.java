package ma.octo.assignement.dto;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
public class VersementDto implements Serializable {

  private String nrCompteBeneficiaire;
  private String nom_prenom_emetteur;
  private BigDecimal montantVersement;
  private Date date;

}
