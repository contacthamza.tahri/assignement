package ma.octo.assignement.dto;

import lombok.Data;
import java.io.Serializable;
import java.util.Date;

@Data
public class UtilisateurDto implements Serializable {

  private String username;

  private String gender;

  private String lastname;

  private String firstname;

  private Date birthdate;

}
