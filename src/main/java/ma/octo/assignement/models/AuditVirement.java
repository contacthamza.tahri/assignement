package ma.octo.assignement.models;

import lombok.Data;
import ma.octo.assignement.utils.EventType;

import javax.persistence.*;

@Entity
@Data
@Table(name = "AUDIT_VIREMENT")
public class AuditVirement {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(length = 100)
  private String message;

  @Enumerated(EnumType.STRING)
  private EventType eventType;


}
