package ma.octo.assignement.models;

import lombok.Data;
import lombok.ToString;
import ma.octo.assignement.utils.EventType;

import javax.persistence.*;

@Entity
@Data
@ToString
@Table(name = "AUDIT_VERSEMENT")
public class AuditVersement {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  @Column(length = 100)
  private String message;

  @Enumerated(EnumType.STRING)
  private EventType eventType;


}
